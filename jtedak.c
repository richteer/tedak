#include "jtedak.h"
#include "tedak.h"

JNIEXPORT jint JNICALL Java_tedak_TedakWrapper_tk_1init
  (JNIEnv *env, jobject jobj, jstring jserv, jstring jport)
{
	int ret_val = -1;
	char *server = (char*) (*env)->GetStringUTFChars(env,jserv,0);
	char *port   = (char*) (*env)->GetStringUTFChars(env,jport,0);

	ret_val = tk_init(server,port);

	(*env)->ReleaseStringUTFChars(env,jserv,server);
	(*env)->ReleaseStringUTFChars(env,jport,port);

	return ret_val;
}

JNIEXPORT jint JNICALL Java_tedak_TedakWrapper_tk_1send__CI_3B
  (JNIEnv *env, jobject obj, jchar jtype, jint jsize, jbyteArray jbody)
{
	printf("Received %d, %d\n",jtype,jsize);
	tk_packet_t pkt = {0};

	char *body = (char*) (*env)->GetByteArrayElements(env,jbody,0);
	tk_pack((unsigned char) jtype, (unsigned) jsize, body, &pkt);

	(*env)->ReleaseByteArrayElements(env,jbody,(jbyte*) body, 0);

	return tk_send(&pkt);
}

JNIEXPORT jbyteArray JNICALL Java_tedak_TedakWrapper_tk_1recv
  (JNIEnv *env, jobject obj)
{
	tk_packet_t pk;
	jbyteArray ret = (*env)->NewByteArray(env, sizeof(tk_packet_t));

	tk_recv(&pk);

	(*env)->SetByteArrayRegion(env, ret, 0, sizeof(tk_packet_t), (const jbyte*) &pk);

	return ret;

}

/*JNIEXPORT jint JNICALL Java_tedak_TedakWrapper_tk_1send
  (JNIEnv *env, jobject obj, jbyteArray jpk)
{
	tk_packet_t pk;
	printf("this is being run\n");
	return tk_send(&pk);
}*/

#include <sys/socket.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <netdb.h>

#define DEBUG 1

int outSock;
int inSock;
int inPort = 5555;

struct sockaddr tk_server;
socklen_t tk_socksize;



/* Set up the socket to communicate on, and initialize the global variables.
 *   NOTE: THIS IS REQUIRED
 * Takes in a string for the server IP/hostname, and the port to connect to,
 * returns a non-zero error code if something goes wrong. */
int tk_init(char* server, char* port)
{
	struct addrinfo hints, *servinfo, *p;
	int rv;
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;
	char tempPort[6] = "5555"; // Hard coded for now
	
	memset(&hints,0,sizeof(hints));


	if (/*(NULL == server) ||*/ (NULL == port))
	{
		fprintf(stderr,"Invalid server or port\n");
		return -1;
	}


    if ((rv = getaddrinfo(server,port,&hints,&servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return -2;
    }

	for (p = servinfo;
		p != NULL;
		p = p->ai_next)
	{
		if (-1 != (outSock = socket(p->ai_family, p->ai_family, p->ai_protocol)))
			break;
	}
	if (NULL == p)
	{
		fprintf(stderr, "Could not create a socket!\n");
		return -3;
	}



	memcpy(&tk_server,p->ai_addr,p->ai_addrlen);
	tk_socksize = p->ai_addrlen;

	freeaddrinfo(servinfo);

	/* Create the listening socket */


    if ((rv = getaddrinfo(NULL,tempPort,&hints,&servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return 2;
    }

	for (p = servinfo; p != NULL; p = p->ai_next)
	{
		if (-1 != (inSock = socket(p->ai_family, p->ai_family, p->ai_protocol)))
			break;
	}
	if (NULL == p)
	{
		fprintf(stderr, "Could not create a socket!\n");
		return 3;
	}


	if (-1 == bind(inSock, p->ai_addr, p->ai_addrlen))
	{
		perror("Could not bind");
		close(inSock);
		return 4;
	}

	freeaddrinfo(servinfo);



	return 0;
}

void tk_close()
{
	close(outSock);
	close(inSock);
}


/* Send a packet constructed with tk_pack over the wire
 * Returns the number of bytes sent, or -1 if there is an error */
int tk_send(tk_packet_t *pk)
{
	int ret;
	if (NULL == pk)
	{
		#if DEBUG
		fprintf(stderr,"Tried to send NULL packet\n");
		#endif
		return -1;

	}

	ret = sendto(outSock,pk,sizeof(tk_packet_t),0, &tk_server, tk_socksize); 

	if (-1 == ret)
		fprintf(stderr,"Failed to send packet: %s\n",strerror(errno));

//	free(buf);
	return ret;
}

int tk_recv(tk_packet_t *pk)
{
	int ret;
	struct sockaddr sa;
	socklen_t sl;


	ret = recvfrom(outSock,pk,
		sizeof(tk_packet_t),
		0,&sa,&sl);
	if (-1 == ret) perror("Read failed");

	return ret;
}


/* Construct a packet to send, returns nonzero if something went wrong. */
int tk_pack(tk_type_t type, tk_size_t size, tk_body_t body, tk_packet_t *ret)
{
/* Packet Structure:
[type][size][crc][body]
*/

	ret->tk_type = type;
	ret->tk_size = size;
	ret->tk_crc  = 0;

	memcpy(ret->tk_body, body, size);

	tk_pack_crc(ret);
	return 0;
}


/* Calcuates a CRC on the packet data, using the type, size, and data field.
 * (i.e. everything but the CRC field) */
void tk_pack_crc(tk_packet_t *ret)
{
	int i;
	unsigned char crc = 0;
	for (i = 0; i < sizeof(tk_type_t); i++)
		crc = crc ^ ((unsigned char*) &ret->tk_type)[i];

	for (i = 0; i < sizeof(tk_size_t); i++)
		crc = crc ^ ((unsigned char*) &ret->tk_size)[i];

	for (i = 0; i < ret->tk_size; i++)
		crc = crc ^ ((unsigned char*) ret->tk_body)[i];

	ret->tk_crc = crc;
	return;
}

package tedak;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.Date;
import javax.swing.*;
import sun.audio.*;

public class TedakGUI implements ActionListener
{
    private JFrame frame;
    private JPanel container;
    
    private JPanel textPanel;
    private JTextPane text;
    private JTextPane users;
    private JScrollPane textScroller;
    private JScrollPane userScroller;
    
    private JPanel sendPanel;
    private JTextField message;
    private JButton sendButton;
    
    private JMenuBar menuBar;
    private JMenu file;
    private JMenuItem clear;
    private JMenuItem save;
    private JMenuItem logout;
    private JMenuItem exit;
    private JMenu options;
    private JMenuItem sound;
    private JMenuItem timestamp;
    private boolean soundOn;
    private boolean timestampOn;
    
    private String userName;
    
    private String messageHistory;
    
    private TedakWrapper tw;
    private TedakListener tl;
    
    public TedakGUI(String addr)
    {
        tw = new TedakWrapper(addr,"18888");
        login();
        tl.run();
    }
    public void login()
    {
        JTextField userField = new JTextField(8);
        JTextField passwordField = new JPasswordField(8);
        JPanel login = new JPanel();
        login.add(new JLabel("Username:"));
        login.add(userField);
        login.add(new JLabel("Password:"));
        login.add(passwordField);
        int option = JOptionPane.showConfirmDialog(null, login, "TEDAK: Please Log In.", JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION)
        {
            if(checkLogin(userField.getText(),passwordField.getText()))
            {
                //get other users in the chat from the server
                //currently an array of just 3 null strings
                String[] otherUsers = new String[3];
                buildGUI(userField.getText(), otherUsers);
            }
            else
            {
                System.exit(0);
            }
        }
    }
    public void buildGUI(String user, String otherUsers[])
    {
        messageHistory = "";
        
        text = new JTextPane();
        text.setEditable(false);
        text.setContentType("text/html");
        //basically a blank text box to make it stretched out to the right size
        text.setText("<html><span style=\"color:#00ffff;\">_______________________________________________________________________<br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br></span></html>");
        textScroller = new JScrollPane(text);
        
        users = new JTextPane();
        users.setContentType("text/html");
        users.setEditable(false);
        userScroller = new JScrollPane(users);
        textPanel = new JPanel(new BorderLayout());
        
        textPanel.add(BorderLayout.WEST, textScroller);
        textPanel.add(BorderLayout.CENTER, userScroller);
        
        message = new JTextField(50);
        message.addActionListener(this);
        sendButton = new JButton("Send");
        sendButton.addActionListener(this);
        sendPanel = new JPanel(new BorderLayout());
        sendPanel.add(BorderLayout.WEST, message);
        sendPanel.add(BorderLayout.EAST, sendButton);
        
        file = new JMenu("File");
        clear = new JMenuItem("Clear");
        file.add(clear);
        clear.addActionListener(this);
        save = new JMenuItem("Save");
        file.add(save);
        save.addActionListener(this);
        logout = new JMenuItem("Logout");
        file.add(logout);
        logout.addActionListener(this);
        exit = new JMenuItem("Exit");
        file.add(exit);
        exit.addActionListener(this);
        options = new JMenu("Options");
        sound = new JMenuItem("Turn Off Sound");
        options.add(sound);
        sound.addActionListener(this);
        timestamp = new JMenuItem("Turn Off Timestamp");
        options.add(timestamp);
        timestamp.addActionListener(this);
        menuBar = new JMenuBar();
        menuBar.add(file);
        menuBar.add(options);
        
        container = new JPanel(new BorderLayout());
        container.add(BorderLayout.NORTH, textPanel);
        container.add(BorderLayout.SOUTH, sendPanel);
        frame = new JFrame("TEDAK Chat System");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setJMenuBar(menuBar);
        frame.getContentPane().add(container);
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);
        
        container.setBackground(Color.yellow);
        sendPanel.setBackground(Color.yellow);
        textPanel.setBackground(Color.yellow);
        users.setBackground(Color.lightGray);
        text.setBackground(Color.cyan);
        sendButton.setBackground(Color.pink);
        
        userName = user;
        soundOn = true;
        timestampOn = true;
        String updateUserText = "<html><b><span style=\"color:#0000cd;\">" + userName + "</span>";
        for(int i = 0; i < otherUsers.length && i < 15; i++) //allows a maximum of 15 other users
        {
            updateUserText = updateUserText + "<span style=\"color:#ff0000;\"><br></br>" + otherUsers[i] + "</span>";
        }
        updateUserText = updateUserText + "</b></html>";
        users.setText(updateUserText);
    }
    public boolean checkLogin(String user, String pass)
    {
        //compare usernames and passwords to the server database
        String msg = user;// + "," + pass;
        tw.tk_send(tw.TK_LOGIN, msg.length(), msg.getBytes());
        //should expect a packet back to receive, do while needs to be added
        //for now, just assumes login info is correct
        return true;
    }
    public void newMessage(String user, String message)
    {
        DateFormat dFormat = new SimpleDateFormat("HH:mm:ss");
        String time = ":  ";
        if(timestampOn)
        {
            time = " (" + dFormat.format(new Date()) + "):  ";
        }
        if(user.equals(userName))
        {
            messageHistory = messageHistory + "<b><span style=\"color:#0000cd;\">" + user + "</span></b>"+ time + message + "<br></br>";
        }
        else
        {
            messageHistory = messageHistory + "<b><span style=\"color:#ff0000;\">" + user + "</span></b>"+ time + message + "<br></br>";
        }
        text.setText("<html>" + messageHistory + "</html>");
    }
    public void sendMessage(String user, String message)
    {
        newMessage(user, message);
        playSound(); //may be removed later
        //Need to send message to server
        String usermessage = "<b><span style=\"color:#ff0000;\">" + user + "</span></b>" + message;
        tw.tk_send(tw.TK_MSG, message.length(), message.getBytes());
    }
    public void receiveMessage(String user, String message)
    {
        newMessage(user, message);
        playSound();
    }
    public void playSound()
    {
        if(soundOn)
        {
            try
            {
                InputStream in = new FileInputStream("sound.wav");
                AudioStream as = new AudioStream(in);
                AudioPlayer.player.start(as);
            }
            catch(FileNotFoundException fnfe)
            {
                System.err.println("Error: sound file not found.");
            }
            catch(IOException ioe)
            {
                System.err.println("Error loading sound.");
            }
        }
    }
    public void actionPerformed(ActionEvent event)
    {
        if(event.getSource().equals(sendButton) || event.getSource().equals(message))
        {
            if(!(message.getText().equals("")))
            {
                sendMessage(userName, message.getText());
                message.setText("");
            }
        }
        else if (event.getSource().equals(clear))
        {
            text.setText("");
            messageHistory = "";
        }
        else if (event.getSource().equals(save))
        {
            try
            {
                String filename = "";
                do
                {
                    filename = JOptionPane.showInputDialog(null, "Please enter a file name.");
                } while(filename.equals(""));
                filename+=".html";
                File file1 = new File(filename);
                BufferedWriter output = new BufferedWriter(new FileWriter(file1));
                output.write(text.getText());
                output.close();
            }
            catch(IOException e)
            {
                System.err.println("IO ERROR");
            }
        }
        else if (event.getSource().equals(logout))
        {
            frame.dispose();
            login();
        }
        else if (event.getSource().equals(exit))
        {
            System.exit(0);
        }
        else if (event.getSource().equals(sound))
        {
            if(soundOn)
            {
                soundOn = false;
                sound.setText("Turn On Sound");
            }
            else
            {
                soundOn = true;
                sound.setText("Turn Off Sound");
            }
        }
        else if (event.getSource().equals(timestamp))
        {
            if(timestampOn)
            {
                timestampOn = false;
                timestamp.setText("Turn On Timestamp");
            }
            else
            {
                timestampOn = true;
                timestamp.setText("Turn Off Timestamp");
            }
        }
    }
}

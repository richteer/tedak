package tedak;

public class TedakWrapper {

	public native int tk_init(String server, String port);
	public native int tk_send(char type, int size, byte[] body);
	public native byte[] tk_recv();

	public final char TK_ERROR = (char) 0;
	public final char TK_PING = (char) 1;
	public final char TK_ACK = (char) 2;
	public final char TK_LOGIN = (char) 4;
	public final char TK_LOGOUT = (char) 8;
	public final char TK_MSG = (char) 16;
	public final char TK_USERS = (char) 32;
	public final char TK_BADCRC = (char) 64;
	public final char TK_SRVCLOSE = (char) 99;


	static {
		System.loadLibrary("tedak");
	} 

	public TedakWrapper(String nserv, String nport) {
		this.tk_init(nserv,nport);
	}

	public int send(String message) {
		return this.tk_send((char)16,message.length(),message.getBytes());
	}
}
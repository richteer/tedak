#define PK_ERROR  0 
#define PK_PING   1 << 0
#define PK_ACK	  1 << 1
#define PK_LOGIN  1 << 2
#define PK_LOGOUT 1 << 3
#define PK_MSG    1 << 4
#define PK_USERS  1 << 5
#define PK_BADCRC 1 << 6


#define TK_MAX_BODY     256
#define TK_PADDING_SIZE 2

typedef unsigned char tk_type_t;
typedef unsigned int  tk_size_t;
typedef unsigned char tk_crc_t;
typedef char          tk_body_t[TK_MAX_BODY];

typedef struct 
{
	tk_size_t tk_size;
	tk_type_t tk_type;
	tk_crc_t  tk_crc;
	char	  __padding[TK_PADDING_SIZE];
	tk_body_t tk_body;
} tk_packet_t;

int tk_init(char *server, char *port);


/* Sends a tk_packet_t to the connected server */
int tk_send(tk_packet_t *pk);

/* Receive a tk_packet_t from the connected server */
int tk_recv(tk_packet_t *pk);

/* Generates a tk_packet_t for easy sending */
/*  NOTE, the size field is the size of the buffer, NOT the rest*/
int tk_pack(tk_type_t type, tk_size_t size, tk_body_t body, tk_packet_t *ret);

/* Calculate the CRC on the packet data */
void tk_pack_crc(tk_packet_t *ret);

/* Clean up the leftover allocated memory, and close the socket in use. */
void tk_close();

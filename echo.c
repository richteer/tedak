#include "tedak.h"

#include <sys/socket.h>
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <netdb.h>

int sockfd;

void handle_sigint(int x)
{
	close(sockfd);
	exit(1);
}


int main(int argc, char** argv)
{
	struct addrinfo hints,*servinfo, *p;
	socklen_t tk_socksize;
	tk_packet_t pk;

	signal(SIGINT,handle_sigint);

	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE;

	memset(&hints,0,sizeof(hints));

	getaddrinfo(NULL,"12345",&hints,&servinfo);

	for (p = servinfo; p != NULL; p = p->ai_next)
		if (-1 != (sockfd = socket(p->ai_family,p->ai_family,0)))
			break;

	if (-1 == sockfd)
	{
		perror("socket error");
		return 1;
	}


	if (-1 == bind(sockfd, p->ai_addr, p->ai_addrlen))
	{
		perror("echo: bind error");
		close(sockfd);
		return 2;
	}

	tk_socksize = p->ai_addrlen;

	printf("Socket: %d\n",sockfd);

	freeaddrinfo(servinfo);


	while (1)
	{
		memset(&pk,0,sizeof(pk));
		recvfrom(sockfd,&pk,512,0,p->ai_addr,&tk_socksize);
		perror("asdasd");
		printf("message: %s\n",pk.tk_body);
	}

}
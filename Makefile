CC=gcc
JC=javac
CFLAGS=-Wall
JNI_INCLUDE=-I/opt/java/include -I/opt/java/include/linux


all: clean tedak.o libtedak.so.1 libtedak.so.2 libtedak.so Derp
	@echo "Built all"

libtedak.so: libtedak.so.2
	@cp $< $@

libtedak.so.1: tedak.c tedak.h
	@echo "Creating the shared library"
	@${CC} -c ${CFLAGS} $< -fpic -o tedak.fpic.o
	@${CC} -shared -o $@ tedak.fpic.o
	@rm -f tedak.fpic.o

libtedak.so.2: jtedak.c jtedak.h tedak.o libtedak.so.1
	@echo "Creating the Java-crapatible library"
	@${CC} -c ${CFLAGS} $< -fpic ${JNI_INCLUDE} -o jtedak.fpic.o
	@${CC} -shared jtedak.fpic.o -o $@
	@rm -f jtedak.fpic.o

tedak/TedakWrapper.class: tedak/TedakWrapper.java
	@echo "Building the TedakWrapper class"
	@${JC} $<

jtedak.h: tedak/TedakWrapper.class
	@echo "Generating the header..."
	@javah -jni tedak.TedakWrapper
	@mv tedak_TedakWrapper.h $@

tedak.o: tedak.c tedak.h
	@echo "Building tedak.o..."
	@${CC} -c ${CFLAGS} $< -o $@

TedakGUI: tedak/TedakGUI.java tedak/TedakMain.java tedak/TedakListener.java
	@echo "Building the GUI"
	@${JC} $^ 
	
test: test.c tedak.o
	@echo "Building test..."
	@${CC} ${CLAGS} $^ -o $@

echo: echo.c tedak.o
	@echo "Building Echo..."
	@${CC} ${CFLAGS} $^ -o $@

Derp: Derp.java
	javac Derp.java

clean:
	@echo "Cleaning directory..."
	@rm -f *.o *.so.* *.so *.class tedak/*.class
